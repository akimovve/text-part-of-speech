package ru.valery;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    private static final Pattern ADJECTIVE_PATTERN = Pattern.compile("\\b[А-Яа-я]+(ый|ий|ой|ая|яя|ое|ее|ые|ие|ых|их|им|ым|ом|ем)\\b");
    private static final Pattern ADVERB_PATTERN = Pattern.compile("\\b[А-Яа-я]+(о|е|ы|и|у|ю|й|ою|ею)\\b");
    private static final Pattern VERB_PATTERN = Pattern.compile("\\b[А-Яа-я]+(ит|ть|ться|чь|чься|жь|жься|ться|ть|тся|ться|чь|чься)\\b");

    public static void main(final String[] args) {
        try {
            final String text = new String(Files.readAllBytes(Paths.get("text.txt")));
            int[] result = calcWords(text);
            System.out.println(result);
        } catch (IOException e) {
            System.out.println("Ошибка чтения файла" + e.getMessage());
        }
    }

    public static int[] calcWords(final String text) {
        final Matcher adjectiveMatcher = ADJECTIVE_PATTERN.matcher(text);
        final Matcher adverbMatcher = ADVERB_PATTERN.matcher(text);
        final Matcher verbMatcher = VERB_PATTERN.matcher(text);

        int adjectiveCount = 0;
        while (adjectiveMatcher.find()) {
            adjectiveCount++;
        }
        int adverbCount = 0;
        while (adverbMatcher.find()) {
            adverbCount++;
        }
        int verbCount = 0;
        while (verbMatcher.find()) {
            verbCount++;
        }

        int[] res = new int[3];
        res[0] = adjectiveCount;
        res[1] = adverbCount;
        res[2] = verbCount;
        return res;
    }

}