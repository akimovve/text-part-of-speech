package ru.valery;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static ru.valery.Main.calcWords;

class MainTest {

    @Test
    @DisplayName("Должно быть 3 глагола")
    void shouldFind3Verbs() {
        final String text = "Ходить, бегать, плавать";
        int[] actual = calcWords(text);
        int[] expected = new int[]{0, 0, 3};
        assertArrayEquals(expected, actual);
    }

    @Test
    @DisplayName("Должно быть 1 прил., 1 нар., 1 гл.")
    void shouldFind1Adjective1Adverb1Verb() {
        final String text = "Сходить, опасно, привлекательная";
        int[] actual = calcWords(text);
        int[] expected = new int[]{1, 1, 1};
        assertArrayEquals(expected, actual);
    }

}
